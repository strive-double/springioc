package com.dnn.springioc.factory;

import com.dnn.springioc.mapper.HelloMapper;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @author dnn
 * @date 2021/11/22 16:41
 */
public class  BeanFactory {
    private static Properties properties;
    private static Map<String,Object> cache =new HashMap<>();
    static {
        properties=new Properties();
        try {
            properties.load(BeanFactory.class.getClassLoader().getResourceAsStream("factory.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static Object getMapper(String beanName) {
        //先半段缓存中是否存在这个bean
        if(!cache.containsKey(beanName)){
            synchronized (BeanFactory.class){
                if(!cache.containsKey(beanName)){
                    //将been存入缓存
                    //反射机制创建对象
                    try {
                        String value = properties.getProperty(beanName);
                        Object obj = Class.forName(value).newInstance();
                        cache.put(beanName,obj);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            //将been存入缓存
            //反射机制创建对象
            try {
                String value = properties.getProperty(beanName);
                Object obj = Class.forName(value).newInstance();
               cache.put(beanName,obj);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return cache.get(beanName);

    }

}
