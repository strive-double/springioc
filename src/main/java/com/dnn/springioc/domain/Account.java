package com.dnn.springioc.domain;

import com.dnn.springioc.myspring.anno.Autowired;
import com.dnn.springioc.myspring.anno.Component;
import com.dnn.springioc.myspring.anno.Qualifier;
import com.dnn.springioc.myspring.anno.Value;
import lombok.Data;

//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Component;
//import org.springframework.stereotype.Repository;


/**
 * @author dnn
 * @date 2021/11/22 20:38
 */
@Data
@Component
public class Account {
    @Value("1")
    private Integer id;
    @Value("张三")
    private String name;
    @Value("11")
    private Integer age;
    @Autowired
    @Qualifier("order")
    private Order order;
}
