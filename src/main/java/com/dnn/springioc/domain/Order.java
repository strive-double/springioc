package com.dnn.springioc.domain;

import com.dnn.springioc.myspring.anno.Component;
import com.dnn.springioc.myspring.anno.Value;
import lombok.Data;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.stereotype.Component;

/**
 * @author dnn
 * @date 2021/11/22 20:55
 */
@Data
@Component("order")
public class Order {
    @Value("XXX123")
    private String orderId;
    @Value("1000.0")
    private Float price;

}
