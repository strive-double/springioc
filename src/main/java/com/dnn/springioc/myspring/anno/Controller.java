package com.dnn.springioc.myspring.anno;

import java.lang.annotation.*;
/**
 * 页面交互
 * @author dnn
 *
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Controller {
    String value() default "";
}
