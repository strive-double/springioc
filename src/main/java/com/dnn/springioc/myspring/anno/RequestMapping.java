package com.dnn.springioc.myspring.anno;

import java.lang.annotation.*;
/**
 * 请求url
 * @author dnn
 *
 */
@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequestMapping {
    String value() default "";
}
