package com.dnn.springioc.myspring.anno;

import java.lang.annotation.*;
/**
 * 请求参数映射
 * @author dnn
 *
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequestParam {
    String value() default "";
    boolean required() default true;
}
