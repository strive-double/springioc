package com.dnn.springioc.myspring;

/**
 * @author dnn
 * @date 2021/11/23 15:44
 */
public class Test {
    public static void main(String[] args) {
        MyAnnotationConfigApplicationContext applicationContext = new MyAnnotationConfigApplicationContext("com.dnn.springioc.myspring.entity");
        System.out.println(applicationContext.getBean("account"));
        System.out.println(applicationContext.getBean("order"));
    }
}
