package com.dnn.springioc.myspring;

import com.dnn.springioc.myspring.anno.Autowired;
import com.dnn.springioc.myspring.anno.Component;
import com.dnn.springioc.myspring.anno.Qualifier;
import com.dnn.springioc.myspring.anno.Value;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/**
 * @author dnn
 * @date 2021/11/23 15:10
 */

public class MyAnnotationConfigApplicationContext {
    private Map<String, Object> ioc = new HashMap<>();

    public MyAnnotationConfigApplicationContext(String pack) {
        //遍历包，找到目标类(原材料)
        Set<BeanDefinition> beanDefinitions = findBeanDefinitions(pack);
        //根据原材料创建bean
        createObject(beanDefinitions);
        //自动装载
        autowireObject(beanDefinitions);

    }
    public void autowireObject(Set<BeanDefinition> beanDefinitions){
        Iterator<BeanDefinition> iterator = beanDefinitions.iterator();
        while (iterator.hasNext()) {
            BeanDefinition beanDefinition = iterator.next();
            Class clazz = beanDefinition.getBeanClass();
            Field[] declaredFields = clazz.getDeclaredFields();
            for (Field declaredField : declaredFields) {
                Autowired annotation = declaredField.getAnnotation(Autowired.class);
                if(annotation!=null){
                    Qualifier qualifier = declaredField.getAnnotation(Qualifier.class);
                    if(qualifier!=null){
                        //byName
                        try {
                            String beanName = qualifier.value();
                            Object bean = getBean(beanName);
                            String fieldName = declaredField.getName();
                            String methodName="set"+fieldName.substring(0,1).toUpperCase()+fieldName.substring(1);
                            Object object = getBean(beanDefinition.getBeanName());
                            Method method = clazz.getMethod(methodName, declaredField.getType());
                            method.invoke(object,bean);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    }else {
                        //byType
                    }
                }

            }
        }

    public Object getBean(String beanName) {
        return ioc.get(beanName);
    }

    public void createObject(Set<BeanDefinition> beanDefinitions) {
        Iterator<BeanDefinition> iterator = beanDefinitions.iterator();
        while (iterator.hasNext()) {
            BeanDefinition beanDefinition = iterator.next();
            Class clazz = beanDefinition.getBeanClass();
            String beanName = beanDefinition.getBeanName();

            try {
                //创建的对象
                Object object = clazz.getConstructor().newInstance();
                //完成属性的赋值
                Field[] declaredFields = clazz.getDeclaredFields();
                for (Field declaredField : declaredFields) {
                    Value valueAnno = declaredField.getAnnotation(Value.class);
                    if (valueAnno != null) {
                        String value = valueAnno.value();
                        String fieldName = declaredField.getName();
                        String methodName = "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
                        Method method = clazz.getMethod(methodName, declaredField.getType());
                        //完成类型的转换
                        Object val = null;
                        switch (declaredField.getType().getName()) {
                            case "java.lang.Integer":
                                val = Integer.parseInt(value);
                                break;
                            case "java.lang.String":
                                val=value;
                                break;
                            case "java.lang.Float":
                               val = Float.parseFloat(value);
                               break;
                        }
                        method.invoke(object,val);
                    }
                }
                //存入缓存
                ioc.put(beanName, object);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public Set<BeanDefinition> findBeanDefinitions(String pack) {
        //1.获取包下的所有类
        Set<Class<?>> classes = MyTools.getClasses(pack);
        Set<BeanDefinition> beanDefinitions = new HashSet<>();
        Iterator<Class<?>> iterator = classes.iterator();
        while (iterator.hasNext()) {
            //2.遍历这些类，找到添加了注解的类
            Class<?> clazz = iterator.next();
            Component annotation = clazz.getAnnotation(Component.class);
            if (annotation != null) {
                //获取Component注解的值
                String beanName = annotation.value();
                if ("".equals(beanName)) {
                    //获取类名首字母小写
                    String className = clazz.getName().replaceAll(clazz.getPackage().getName() + ".", "");
                    beanName = className.substring(0, 1).toLowerCase() + className.substring(1);
                }
                //3.将这些类封装成BeanDefinition,装载到集合中
                beanDefinitions.add(new BeanDefinition(beanName, clazz));

            }

        }

        return beanDefinitions;
    }
}
