package com.dnn.springioc.myspring.entity;

import com.dnn.springioc.myspring.anno.Autowired;
import com.dnn.springioc.myspring.anno.Component;
import com.dnn.springioc.myspring.anno.Qualifier;
import com.dnn.springioc.myspring.anno.Value;
import lombok.Data;

/**
 * @author dnn
 * @date 2021/11/23 15:46
 */
@Component
@Data
public class Account {
    @Value("1")
    private Integer id;
    @Value("张三")
    private String name;
    @Value("12")
    private Integer age;
    @Autowired
    @Qualifier("myOrder")
    private Order order;
}
