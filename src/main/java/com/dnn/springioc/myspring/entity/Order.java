package com.dnn.springioc.myspring.entity;

import com.dnn.springioc.myspring.anno.Component;
import com.dnn.springioc.myspring.anno.Value;
import lombok.Data;

/**
 * @author dnn
 * @date 2021/11/23 16:19
 */
@Data
@Component("myOrder")
public class Order {
    @Value("xxx123")
    private String orderId;
    private Float price;
}
